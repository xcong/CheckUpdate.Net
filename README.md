### CheckUpdate.Net


UpdateFileServerToQiNiu 是依赖七牛云存储做更新的一个程序，取代原来利用服务器部署iis服务功能。通过七牛云CDN网络可以加快下载速度，新用户有每月10GB免费流量。
[去注册](https://s.qiniu.com/VvINR3)

![CDN价格](15320.jpg)
![程序界面](8917.jpg)


### 介绍

>**CheckUpdate.Net**是.Net C/S下一个检查更新程序。现有的检查更新方式多种多样，更新程序也大不相同。有个比较出名的<a href="http://www.cnblogs.com/zhuweisky/p/3927224.html" target="_blank">OSAU</a>（参考了他的界面），但是有10个链接限制。微软也有比较方便的ClickOnce。自己也尝试了其他的，发现没有合适的就决定自己写一个。
考虑到复用，在.Net Framework2.0下开发。主要就是利用WebClient下载服务器网站目录下的文件，安全性暂没有考虑，比较适用于小型项目。

>UpdateFileClient.exe/UpdateFile.xml/UpdateFileCommon.dll/UpdateFileClient.exe.config[可选] 为客户端需要用到的文件

### 注意事项：

1.UpdateFileClient.exe依赖于.Net Framwork 2.0，如果主程序的.Net Framwork版本高于2.0，需要添加UpdateFileClient.exe.config文件(默认是兼容4.0)。
如果主程序的.Net Framwork版本为2.0，请删除UpdateFileClient.exe.config文件。
>.Net Framwork 4.0是新的CLR，无法兼容旧版CLR。该方案主要兼容在Windows Xp下单独安装了.Net Framework 4.0 版本。
Windows Vista/Win7 已经分别安装.Net Framework 2.0与3.5，具有CLR 2.0特性，需要删除App.config。

### 主要功能

+ 支持单个或多个文件更新
    读取服务端XML配置文件，获取需要修改或新增的文件，然后进行下载，下载完成之后，更新本地版本。
    新增本地md5文件和xml文件中Md5值比较，重复文件不重复下载，节省流量。
+ 支持更新更新程序本身
     通过配置本地的XML文件，放置更新程序的目录。主程序启动时，进行检查，处理。
+ 服务端程序依赖七牛云，通过七牛云CDN网络更快下载文件。




### 使用方式

1. 打开UpdateFileServerToQiNiu.exe配置服务端文件，填写七牛云相关信息，配置文件列表。
    密钥管理：[https://portal.qiniu.com/developer/user/key](https://portal.qiniu.com/developer/user/key)
    使用前创建空间，绑定域名。完成域名绑定后，点击域名设置响应头Access-Control-Allow-Methods，值为*。

2. 将主程序运行需要的文件通过XML配置起来
  将Update.xml、UpdateFileClient.exe、UpdateFileCommon.dll添加到启动程序相同目录，主程序需要引用UpdateFileCommon.dll。

  将启动程序需要更新的文件通过XML进行配置，放在File节点下，初始版本为1。
  
  配置服务端XML所在路径，当前版本、版本对应的值、临时文件夹、更新程序名称可以采用默认值。
  
3. 实现更新程序的更新

  需要在主程序中添加一行代码，进行检查临时文件夹是否包含更新程序，如果有，进行剪切操作。


```
using UpdateFileCommon;
//发现新的更新程序，进行剪切到根目录
VersionHelper.CutNewUpdateEXE();
```

4. 按需添加检查更新代码
  可以在程序启动时或者点击按钮进行检查更新操作。
  检查更新是弹窗进行提醒，需要传两个参数，一个更新描述，一个是否强制更新。对应XML节点是服务端XML的ReleaseNote和IsMustUpdate。
  NextShowEvent 事件是点击按钮下次提醒需要执行的操作。

  **Winform**
  
```
    //默认的LoginView为登陆窗体
    ApplicationContext context = new ApplicationContext(new LoginView());
    //发现设定的目录存在新的更新程序，进行剪切到根目录
    VersionHelper.CutNewUpdateEXE();

    //检查是否需要下载安装包，不需要下载返回False
    if (!VersionHelper.GetNewVersionToDownloadSetup())
    {
        //检查版本更新
        if (VersionHelper.IsRequiredUpdate())
        {
            string xmlPath = System.AppDomain.CurrentDomain.BaseDirectory + "UpdateFile.xml";
            if (File.Exists(xmlPath))
            {
                //加载XML路径
                XmlDocument doc = new XmlDocument();
                doc.Load(VersionHelper.GetLoaclServerConfigURL(xmlPath));
                //获取值
                var releaseNote = VersionHelper.GetServiceReleaseNote(doc);
                var isMustUpdate = VersionHelper.GetServiceIsMustUpdate(doc);
                PromptingForm form = new PromptingForm(releaseNote, isMustUpdate);
                //赋值委托，点击下次提醒的按钮执行的操作
                form.NextShowEvent += delegate
                {
                    //执行xxx
                };
                context = new ApplicationContext(form);
            }
        }
    }
    
    //do something
     Application.Run(context);
```

   **WPF**
   >WPF中需要特殊处理，在WPF项目中添加Program.cs文件，添加Main方法，项目右键属性，设置Program为启动对象。
   
```
[STAThread]
static void Main()
{
    //发现设定的目录存在新的更新程序，进行剪切到根目录
    VersionHelper.CutNewUpdateEXE();

    //检查是否需要下载安装包，不需要下载返回False
    if (!VersionHelper.GetNewVersionToDownloadSetup())
    {
        //检查版本更新
        if (VersionHelper.IsRequiredUpdate())
        {
            string xmlPath = System.AppDomain.CurrentDomain.BaseDirectory + "UpdateFile.xml";
            if (File.Exists(xmlPath))
            {
                //加载XML路径
                XmlDocument doc = new XmlDocument();
                doc.Load(VersionHelper.GetLoaclServerConfigURL(xmlPath));
                //获取值
                var releaseNote = VersionHelper.GetServiceReleaseNote(doc);
                var isMustUpdate = VersionHelper.GetServiceIsMustUpdate(doc);
                PromptingForm form = new PromptingForm(releaseNote, isMustUpdate);
                //赋值委托，点击下次提醒的按钮执行的操作
                form.NextShowEvent += delegate
                {
                    //执行xxx
                };
                //这里启动PromptingForm窗体
                form.ShowDialog();
            }
        }
    }
    //不需要更新时，默认启动登陆窗体
    else
    {
        LoginView view = new LoginView();
        view.ShowDialog();
    }
}
```


### 联系我们

>CheckUpdate.Net交流群 137867517  <a href="http://jq.qq.com/?_wv=1027&k=ZSipwI" title="version 1.2" target="点击加入群">[点击加群]</a>